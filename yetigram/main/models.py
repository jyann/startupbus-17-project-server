# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    address = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=255)

    name = models.CharField(max_length=255)
    description = models.TextField()
    job_radius = models.PositiveIntegerField()
    days_available = models.CharField(max_length=255)

class Request(models.Model):
    customer = models.ForeignKey(User, on_delete=models.CASCADE)

    address = models.CharField(max_length=255)
    address2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    zip_code = models.CharField(max_length=255)

    title = models.CharField(max_length=255)
    time = models.PositiveIntegerField()
    description = models.TextField()

class RequestedEntertainer(models.Model):
    request = models.ForeignKey(Request, on_delete=models.CASCADE)
    entertainer = models.ForeignKey(User, on_delete=models.CASCADE)
    accepted = models.BooleanField()
    denied = models.BooleanField()

    class Meta:
        unique_together = ('request', 'entertainer')
