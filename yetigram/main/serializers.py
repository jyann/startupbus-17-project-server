from django.contrib.auth.models import User, Group

from rest_framework import serializers

from models import Profile, Request, RequestedEntertainer

class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')

class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    user_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        model = Profile
        fields = (
            'user_id',
            'name',
            'description',
            'job_radius',
            'days_available',

            'address',
            'address2',
            'city',
            'state',
            'zip_code'
        )

# class RequestSerializer(serializers.HyperlinkedModelSerializer):
#     customer_id = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
#
#     class Meta:
#         model = Profile
#         fields = (
#             'customer_id',
#             'title',
#             'time',
#             'description',
#
#             'address',
#             'address2',
#             'city',
#             'state',
#             'zip_code'
#         )
